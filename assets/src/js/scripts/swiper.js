;(function(context) {

	var $document = $(document);
	
	var methods = {
	
		getSwiperWrappers: function() {
			return $('div.swiper-wrapper');
		},
	
		buildSwiper: function(wrapper,options) {
			
			var 
				settings = options || {},
				el = wrapper,
				swiper = el.children('div.swiper'),
				selectNav = el.find('select.swiper-nav'),
				responsive = swiper.data('responsive');
				
				$.extend(settings,swiper.data());
				
				if(responsive) {
					settings.responsive = [responsive];
				}
				
			swiper.slick(settings);
			
			selectNav.on('change',function() {
				swiper.slick('goTo',this.selectedIndex);
			});
			
			
		},
		
		updateTemplate: function() {
			methods
				.getSwiperWrappers()
				.filter(function() {
					return typeof $(this).data('slick') === 'undefined'
				})
				.each(function() {
					methods.buildSwiper($(this));
				});		
		}
	
	}
	
	$document
		.on('updateTemplate.swiper ready',function() {
			methods.updateTemplate();
		});
		
	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = {
			buildSwiper: function(wrapper,options) { methods.buildSwiper(wrapper,options); }
		}
	//CodeKit
	} else if(context) {
		context.swiper = {
			buildSwiper: function(wrapper,options) { methods.buildSwiper(wrapper,options); }
		}
	}	

}(typeof ns !== 'undefined' ? window[ns] : undefined));