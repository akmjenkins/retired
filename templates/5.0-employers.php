<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Employers</h1>
				<span class="hgroup-subtitle">Discover the Benefits</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">Employers</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">
		
			<div class="hgroup center">
				<h2 class="hgroup-title">Featured Employer</h2>
				<span class="hgroup-subtitle">Lorem ipsum dolor sit amet, consectetur.</span>
			</div><!-- .hgroup -->			
			
			<hr />
			
			<div class="featured-block-wrap">
				<div class="featured-block">
					
					<div class="featured-img">
						<div class="lazybg with-img">
							<img src="../assets/dist/images/temp/walmart.png" alt="Walmart logo">	
						</div><!-- .lazybg -->
					</div><!-- .featured-img -->
					
					<div class="featured-content">
						
						<h2>Walmart</h2>
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
						
						<div class="buttons">
							<a href="#" class="button">Learn More</a>
							<a href="#" class="button">
								<span class="badge">12</span>
								Job Opportunities
							</a>
							<a href="#" class="button">Join Now</a>
						</div><!-- .buttons -->
						
					</div><!-- .featured-content -->
					
				</div>
			</div><!-- .featured-block-wrap -->
			
		</div><!-- .sw -->
		
	</section>

	<section class="filter-section">
		
		<div class="filter-bar">
			<div class="sw">

				<div class="filter-bar-left">
				
					<div class="count">
						<span class="num">6</span>
						Resources
					</div><!-- .count -->

				</div><!-- .filter-bar-left -->

				<div class="filter-bar-meta">
					
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
				</div><!-- .filter-bar-meta -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
		
			<div class="sw">
				
				<div class="grid eqh blocks collapse-at-850 blocks">
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button" href="#">
							
								<div>
									<span class="badge">12</span> Job Opportunities
								</div>
							
								<div class="img-wrap">
									<div class="img lazybg with-img" data-src="../assets/dist/images/temp/walmart.png">
										&nbsp;
									</div><!-- .img -->
								</div><!-- .img-wrap -->
								
								<div class="content">
								
									<div class="hgroup">
										<h4 class="hgroup-title">Walmart Canada</h4>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button" href="#">
							
								<div>
									<span class="badge">12</span> Job Opportunities
								</div>
							
								<div class="img-wrap">
									<div class="img lazybg with-img" data-src="../assets/dist/images/temp/walmart.png">
										&nbsp;
									</div><!-- .img -->
								</div><!-- .img-wrap -->
								
								<div class="content">
								
									<div class="hgroup">
										<h4 class="hgroup-title">Walmart Canada</h4>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button" href="#">
							
								<div>
									<span class="badge">12</span> Job Opportunities
								</div>
							
								<div class="img-wrap">
									<div class="img lazybg with-img" data-src="../assets/dist/images/temp/walmart.png">
										&nbsp;
									</div><!-- .img -->
								</div><!-- .img-wrap -->
								
								<div class="content">
								
									<div class="hgroup">
										<h4 class="hgroup-title">Walmart Canada</h4>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .sw -->

		</div><!-- .filter-content -->

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>