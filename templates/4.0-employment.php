<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Employment</h1>
				<span class="hgroup-subtitle">Discover the Benefits</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">Employment</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">

			<div class="main-body">
				<div class="content">
				
					<div class="article-body">
						
						<p>
							There are many reasons why one would consider returning to employment after retirement, and we’re here to help you do just that. With positions tailored to you, our search makes it easier to find what you’re looking for.
						</p>
					
					</div><!-- .article-body -->
				
				</div><!-- .content -->
			</div><!-- .main-body -->
			
			<br />
			<br />
			
		
			<div class="hgroup">
				<h2 class="hgroup-title">Latest Jobs</h2>
				<span class="hgroup-subtitle">Lorem ipsum dolor sit amet, consectetur.</span>
			</div><!-- .hgroup -->
		
			<div class="grid eqh collapse-950">
				<div class="col col-3">
					<a class="item with-button job-item" href="#">
						<div class="pad-20">
							<h4 class="title">HR Manager</h4>
							<span class="meta meta-one">Walmart Canada</span>
							<span class="meta meta-two">St. John's, NL</span>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
							</p>
							
							<span class="button red">Learn More</span>
						</div><!-- .pad-20 -->
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<a class="item with-button job-item" href="#">
						<div class="pad-20">
							<h4 class="title">HR Manager</h4>
							<span class="meta meta-one">Walmart Canada</span>
							<span class="meta meta-two">St. John's, NL</span>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							</p>
							
							<span class="button red">Learn More</span>
						</div><!-- .pad-20 -->
					</a><!-- .item -->

				</div><!-- .col -->
				<div class="col col-3">
					<a class="item with-button job-item" href="#">
						<div class="pad-20">
							<h4 class="title">HR Manager</h4>
							<span class="meta meta-one">Walmart Canada</span>
							<span class="meta meta-two">St. John's, NL</span>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
							</p>
							
							<span class="button red">Learn More</span>
						</div><!-- .pad-20 -->
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->

		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
			<?php include('inc/i-job-search-form.php'); ?>
		</div><!-- .sw -->
	</section>
	
	<section class="filter-section">
		
		<div class="filter-bar">
			<div class="sw">

				<div class="filter-bar-left">
				
					<div class="count">
						<span class="num">6</span>
						Resources
					</div><!-- .count -->

				</div><!-- .filter-bar-left -->

				<div class="filter-bar-meta">
					
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
				</div><!-- .filter-bar-meta -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
		
			<div class="sw">
			
				<div class="grid eqh">
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item with-button job-item" href="#">
							<div class="pad-20">
								<h4 class="title">HR Manager</h4>
								<span class="meta meta-one">Walmart Canada</span>
								<span class="meta meta-two">St. John's, NL</span>
								
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
								</p>
								
								<span class="button red">Learn More</span>
							</div><!-- .pad-20 -->
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item with-button job-item" href="#">
							<div class="pad-20">
								<h4 class="title">HR Manager</h4>
								<span class="meta meta-one">Walmart Canada</span>
								<span class="meta meta-two">St. John's, NL</span>
								
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								</p>
								
								<span class="button red">Learn More</span>
							</div><!-- .pad-20 -->
						</a><!-- .item -->

					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item with-button job-item" href="#">
							<div class="pad-20">
								<h4 class="title">HR Manager</h4>
								<span class="meta meta-one">Walmart Canada</span>
								<span class="meta meta-two">St. John's, NL</span>
								
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
								</p>
								
								<span class="button red">Learn More</span>
							</div><!-- .pad-20 -->
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item with-button job-item" href="#">
							<div class="pad-20">
								<h4 class="title">HR Manager</h4>
								<span class="meta meta-one">Walmart Canada</span>
								<span class="meta meta-two">St. John's, NL</span>
								
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
								</p>
								
								<span class="button red">Learn More</span>
							</div><!-- .pad-20 -->
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item with-button job-item" href="#">
							<div class="pad-20">
								<h4 class="title">HR Manager</h4>
								<span class="meta meta-one">Walmart Canada</span>
								<span class="meta meta-two">St. John's, NL</span>
								
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								</p>
								
								<span class="button red">Learn More</span>
							</div><!-- .pad-20 -->
						</a><!-- .item -->

					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item with-button job-item" href="#">
							<div class="pad-20">
								<h4 class="title">HR Manager</h4>
								<span class="meta meta-one">Walmart Canada</span>
								<span class="meta meta-two">St. John's, NL</span>
								
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
								</p>
								
								<span class="button red">Learn More</span>
							</div><!-- .pad-20 -->
						</a><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->

			
			</div><!-- .sw -->

		</div><!-- .filter-content -->

	</section>

	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>