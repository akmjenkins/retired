<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Why Hire a Retired Worker?</h1>
				<span class="hgroup-subtitle">Lorem ipsum dolor sit amet, consectetur.</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">Retired Workers</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">
		
			<div class="main-body">
			
				<div class="content">
					<div class="article-body">
						<p>
							Whether you're looking for a job or you're interested in hiring someone, Retired Workforce is here to help you. 
							We understand the needs of both parties, and our site is designed to ensure that you find what you're looking for quickly.
						</p>
					</div><!-- .article-body -->
				</div><!-- .content -->
				
				<div class="sidebar">
				
					<div class="sidebar-mod buttons-mod">
					
						<a href="#" class="button">Find a Job</a>
						<a href="#" class="button">Find a Retired Worker</a>						
						
					</div><!-- .sidebar-mod -->
				

				
				</div><!-- .sidebar -->
				
			</div><!-- .main-body -->
		
		</div><!-- .sw -->
		
	</section>
	
	<section class="lazybg half-section left-half" data-src="../assets/dist/images/temp/full-block.jpg">
		<div class="sw">
			<div class="article-body">
				
				<h2>The Changing Workforce</h2>
				
				<p>
					In a number of industries, the province has experienced a major increase in the demand for workers, 
					and meeting this need has proven difficult in many ways. The success of a business depends on its 
					ability to adapt to this situation, and moving away from traditional hiring practices is a great first step.
				</p>
				
			</div><!-- .article-body -->
		</div><!-- .sw -->
	</section><!-- .half-section -->
	
	<section class="split-block">
		
		<div class="split-block-item">
			<div class="split-block-content">
			
				<div class="article-body">				
					<h2>The Answer</h2>
					
					<p>
						Luckily, at the same time Newfoundland and Labrador is experiencing this labour shortage, especially for skilled workers, Canada is also undergoing a shift in the way retirees choose to spend their years after leaving their long-term career. Many people are choosing to return to the workforce, and they currently constitute one of the fastest growing demographics in the country. Retired, ready, and reliable, these people have the experience and ambition to meet the needs of many companies within a number of industries.
					</p>
 
					<p>
						And here at Retired Workforce, we’re working to make the connection.
					</p>
				</div><!-- .article-body -->
			
			</div><!-- .split-block-content -->
		</div><!-- .split-block-item -->
		
		<div class="split-block-item dark-bg">
			<div class="split-block-content">
			
				<div class="article-body">
					<h2>Connect Now</h2>
					
					<p>
						Find the perfect fit for your company today. View our list of retired workers who are ready to work for you.
					</p>
					
					<a href="#" class="button">Log In</a>
					<a href="#" class="button">Sign Up</a>
				</div><!-- .article-body -->
			
			</div><!-- .split-block-content -->
		</div><!-- .split-block-item -->
			
	</section>	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>