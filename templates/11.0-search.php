<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Search Results</h1>
				<span class="hgroup-subtitle">Curabitur in Sapien Finibus</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">Search Results</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">

			<div class="main-body">
				<div class="content">
				
					<div class="article-body">
					
						<p class="excerpt">
							Whether you’re looking for a job or you’re interested in hiring someone, Retired Workforce is here to help you. We understand the needs of both parties, 
							and our site is designed to ensure that you find what you’re looking for quickly.
						</p>
						
						<p>
							In a number of industries, the province has experienced a major increase in the demand for workers, and meeting this need has proven difficult in many ways. 
							The success of a business depends on its ability to adapt to this situation, and moving away from traditional hiring practices is a great first step.
						</p>
					
					</div><!-- .article-body -->
				
				</div><!-- .content -->
			</div><!-- .main-body -->
		
		</div><!-- .sw -->
		
	</section>
	
	<section class="filter-section">
		
		<div class="filter-bar">
			<div class="sw">

				<div class="filter-bar-left">
				
					<div class="count">
						<span class="num">4</span>
						News Results
					</div>
					
				</div><!-- .filter-bar-left -->

				<div class="filter-bar-meta">
					
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
				</div><!-- .filter-bar-meta -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
		
			<div class="sw">
				
				<div class="grid eqh blocks collapse-at-850 blocks">
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button bounce" href="#">
							
								<div class="img-wrap">
									<div class="img lazybg" data-src="../assets/dist/images/temp/featured-block-2.jpg"></div>
								</div><!-- .img-wrap -->
								<div class="content">
								
									<div class="article-meta">
										<time>October 7, 2014</time>
									</div><!-- .article-meta -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button bounce" href="#">
							
								<div class="img-wrap">
									<div class="img lazybg" data-src="../assets/dist/images/temp/featured-block-2.jpg"></div>
								</div><!-- .img-wrap -->
								<div class="content">
								
									<div class="article-meta">
										<time>October 7, 2014</time>
									</div><!-- .article-meta -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->

					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button bounce" href="#">
							
								<div class="img-wrap">
									<div class="img lazybg" data-src="../assets/dist/images/temp/featured-block-2.jpg"></div>
								</div><!-- .img-wrap -->
								<div class="content">
								
									<div class="article-meta">
										<time>October 7, 2014</time>
									</div><!-- .article-meta -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .sw -->

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->

	<section class="filter-section">
		
		<div class="filter-bar">
			<div class="sw">

				<div class="filter-bar-left">
				
					<div class="count">
						<span class="num">3</span>
						Resources
					</div>
					
				</div><!-- .filter-bar-left -->

				<div class="filter-bar-meta">
					
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
				</div><!-- .filter-bar-meta -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
		
			<div class="sw">
				
				<div class="grid eqh blocks collapse-at-850 blocks">
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<span class="block with-button">
								<div class="content">
								
									<div class="tags">
										<a href="#" class="tag button sm red">Category</a>
									</div><!-- .tags -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<a href="#" class="button red">Keep Reading</a>
									
								</div><!-- .content -->
							</span><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<span class="block with-button">
								<div class="content">
								
									<div class="tags">
										<a href="#" class="tag button sm red">Category</a>
									</div><!-- .tags -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<a href="#" class="button red">Keep Reading</a>
									
								</div><!-- .content -->
							</span><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<span class="block with-button">
								<div class="content">
								
									<div class="tags">
										<a href="#" class="tag button sm red">Category</a>
									</div><!-- .tags -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<a href="#" class="button red">Keep Reading</a>
									
								</div><!-- .content -->
							</span><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					
				</div><!-- .grid -->
			
			</div><!-- .sw -->

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->

	<section class="filter-section">
		
		<div class="filter-bar">
			<div class="sw">

				<div class="filter-bar-left">
				
					<div class="count">
						<span class="num">3</span>
						Employers
					</div>
					
				</div><!-- .filter-bar-left -->

				<div class="filter-bar-meta">
					
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
				</div><!-- .filter-bar-meta -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
		
			<div class="sw">
				
				<div class="grid eqh blocks collapse-at-850 blocks">
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button" href="#">
							
								<div>
									<span class="badge">12</span> Job Opportunities
								</div>
							
								<div class="img-wrap">
									<div class="img lazybg with-img" data-src="../assets/dist/images/temp/walmart.png">
										&nbsp;
									</div><!-- .img -->
								</div><!-- .img-wrap -->
								
								<div class="content">
								
									<div class="hgroup">
										<h4 class="hgroup-title">Walmart Canada</h4>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button" href="#">
							
								<div>
									<span class="badge">12</span> Job Opportunities
								</div>
							
								<div class="img-wrap">
									<div class="img lazybg with-img" data-src="../assets/dist/images/temp/walmart.png">
										&nbsp;
									</div><!-- .img -->
								</div><!-- .img-wrap -->
								
								<div class="content">
								
									<div class="hgroup">
										<h4 class="hgroup-title">Walmart Canada</h4>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button" href="#">
							
								<div>
									<span class="badge">12</span> Job Opportunities
								</div>
							
								<div class="img-wrap">
									<div class="img lazybg with-img" data-src="../assets/dist/images/temp/walmart.png">
										&nbsp;
									</div><!-- .img -->
								</div><!-- .img-wrap -->
								
								<div class="content">
								
									<div class="hgroup">
										<h4 class="hgroup-title">Walmart Canada</h4>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button red">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .sw -->

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->

	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>