<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Employment</h1>
				<span class="hgroup-subtitle">Discover the Benefits</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">Employment</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">

			<div class="main-body">
				<div class="content center">
				
					<div class="article-body">
					
						<div class="hgroup">
							<h2 class="hgroup-title">You must log in or sign up to view this page.</h2>
						</div><!-- .hgroup -->
						
						<p>
							Sed gravida justo orci, congue lobortis metus venenatis pellentesque. Vestibulum eget semper nisi, a cursus elit. 
							Donec malesuada consequat sapien, laoreet volutpat massa luctus eu. Proin malesuada lorem id quam lobortis, non pulvinar nunc interdum. 
							Mauris gravida vitae lectus rutrum auctor. In gravida ante a commodo aliquam.
						</p>
					
					</div><!-- .article-body -->
				
				</div><!-- .content -->
			</div><!-- .main-body -->

		
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg grey-bg split-block with-border">
		
		<div class="split-block-item">
			<div class="split-block-content">
			
				<h2>Login</h2>
				<?php include('inc/i-login-form.php'); ?>
			
			</div>
		</div><!-- .split-block-item -->
		
		<div class="split-block-item">
			<div class="split-block-content">
			
				<h2>Register</h2>
				<?php include('inc/i-register-form.php'); ?>
			
			</div><!-- .split-block-content -->
		</div><!-- .split-block-item -->
	</section><!-- .split-block -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>