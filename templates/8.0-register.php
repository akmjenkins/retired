<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Register</h1>
				<span class="hgroup-subtitle">Curabitur in Sapien Finibus</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">Register</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">

			<div class="main-body">
				<div class="content">
				
					<div class="article-body">
					
						<p class="excerpt">
							Quisque a odio vel mauris suscipit venenatis in ut massa. Sed sed magna id ipsum mattis sodales in eu lacus. In ullamcorper mattis rutrum. Cras augue odio, accumsan sed aliquet id, mollis in sem. Vivamus maximus ac arcu nec fringilla. Quisque euismod lacus vel consectetur dignissim.
						</p>
						
						<p>
							Vivamus vel metus vel dolor viverra sodales. Donec in convallis odio. Curabitur in accumsan ante. Donec id auctor elit, eu dictum massa. Donec ut vehicula tortor. Donec at nisi varius, lacinia nunc mollis, scelerisque nisi.
						</p>
					
					</div><!-- .article-body -->
					
					<div class="form-holder">
						<?php include('inc/i-register-form.php'); ?>
					</div>
					
				</div><!-- .content -->
			</div><!-- .main-body -->
		
		</div><!-- .sw -->
		
	</section>

	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>