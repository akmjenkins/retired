<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item dark-overlay" data-src="
			../assets/dist/images/temp/hero/hero-1.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">
		
			<div class="hero-content">
				<div class="sw">
					
					<span class="title">What type of job are you looking for?</span>
					<span class="subtitle">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					</span><!-- .subtitle -->
					
					<div class="hero-search-wrap">
						<?php include('inc/i-job-search-form.php'); ?>
					</div><!-- .hero-search-wrap -->
					
				</div><!-- .sw -->
			</div><!-- .hero-content -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
			<div class="hgroup center">
				<h2 class="hgroup-title">Why Hire a Retired Worker?</h2>
				<span class="hgroup-subtitle">Lorem ipsum dolor sit amet, consectetur.</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
	</section>
	
	<section class="split-block full">
	
		<div class="split-block-item lazybg red-overlay d-bg" data-src="../assets/dist/images/temp/split-block-1.jpg">
			<div class="split-block-content overlay-content">
				
				<div class="swiper-wrapper button-dots">
				
					<div class="swiper" data-dots="true" data-arrows="0">
					
						<div class="swipe-item">
							<h4>Retired workers bring unmatched experience to the workplace.</h4>
						</div><!-- .swipe-item -->
						
						<div class="swipe-item">
							<h4>Lorem ipsum dolor sit amet, consectetur.</h4>
						</div><!-- .swipe-item -->
						
					</div><!-- .swiper -->
					
				</div><!-- .swiper-wrapper -->
				
			</div><!-- .overlay-content -->
		</div><!-- .split-block-item -->
		
		<div class="split-block-item lazybg dark-overlay d-bg" data-src="../assets/dist/images/temp/split-block-2.jpg">
			<div class="split-block-content overlay-content">
				
				<h3>Are you an employer looking to hire retired workers?</h3>
				
				<p>
					Join today and get acess to retired workers who are ready and willing to work for your company.
				</p>
				
				<a href="#" class="button red">Join Today</a>
				
			</div><!-- .overlay-content -->
		</div><!-- .split-block-item -->
			
	</section>
	
	<section>
		<div class="sw">
		
			<div class="hgroup center">
				<h2 class="hgroup-title">Featured Employer</h2>
				<span class="hgroup-subtitle">Lorem ipsum dolor sit amet, consectetur.</span>
			</div><!-- .hgroup -->			
			
			<hr />
			
			<div class="featured-block-wrap">
				<div class="featured-block">
					
					<div class="featured-img">
						<div class="lazybg with-img">
							<img src="../assets/dist/images/temp/walmart.png" alt="Walmart logo">	
						</div><!-- .lazybg -->
					</div><!-- .featured-img -->
					
					<div class="featured-content">
						
						<h2>Walmart</h2>
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
						
						<div class="buttons">
							<a href="#" class="button">Learn More</a>
							<a href="#" class="button">
								<span class="badge">12</span>
								Job Opportunities
							</a>
							<a href="#" class="button">Join Now</a>
						</div><!-- .buttons -->
						
					</div><!-- .featured-content -->
					
				</div>
			</div><!-- .featured-block-wrap -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg red-bg">
		<div class="sw">
		
			<div class="hgroup center">
				<h2 class="hgroup-title">Latest Jobs</h2>
				<span class="hgroup-subtitle">Lorem ipsum dolor sit amet, consectetur.</span>
			</div><!-- .hgroup -->
			
			<div class="grid eqh collapse-950">
				<div class="col col-3">
					<a class="item with-button job-item" href="#">
						<div class="pad-20">
							<h4 class="title">HR Manager</h4>
							<span class="meta meta-one">Walmart Canada</span>
							<span class="meta meta-two">St. John's, NL</span>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
							</p>
							
							<span class="button red">Learn More</span>
						</div><!-- .pad-20 -->
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<a class="item with-button job-item" href="#">
						<div class="pad-20">
							<h4 class="title">HR Manager</h4>
							<span class="meta meta-one">Walmart Canada</span>
							<span class="meta meta-two">St. John's, NL</span>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							</p>
							
							<span class="button red">Learn More</span>
						</div><!-- .pad-20 -->
					</a><!-- .item -->

				</div><!-- .col -->
				<div class="col col-3">
					<a class="item with-button job-item" href="#">
						<div class="pad-20">
							<h4 class="title">HR Manager</h4>
							<span class="meta meta-one">Walmart Canada</span>
							<span class="meta meta-two">St. John's, NL</span>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
							</p>
							
							<span class="button red">Learn More</span>
						</div><!-- .pad-20 -->
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
			
			<div class="center">
				<a href="#" class="button">View All Jobs</a>
			</div>
		
		</div><!-- .sw -->
	</section>
	
	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>