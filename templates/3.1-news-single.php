<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Integer Sagittis et Lacus vel Sagittis</h1>
				<span class="hgroup-subtitle">Discover the Benefits</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">News</a>
				<a href="#">Integer Sagittis et Lacus vel Sagittis</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">
		
			<div class="main-body">
			
				<div class="content">
					<div class="article-body">
					
						<div class="article-thumb lazybg">
							<img src="../assets/dist/images/temp/featured-block-2.jpg" alt="dude with computer">
						</div>
					
						<div class="article-meta">
							<time>October 7, 2014</time>
						</div><!-- .article-meta -->
						
						<p>
							Vivamus aliquet ex eu interdum vehicula. Nam ut ullamcorper ante. Ut bibendum scelerisque est non pellentesque. 
							Fusce fringilla efficitur arcu, nec venenatis ante egestas et. Donec a finibus ligula. Donec non arcu molestie, 
							pretium lorem sed, tincidunt arcu. Integer imperdiet facilisis sem quis sodales. Ut scelerisque viverra nisi at lobortis. 
							Duis justo purus, ultricies at ornare sit amet, commodo nec massa. Duis posuere enim urna, pellentesque ultrices 
							massa consequat non. Nam a facilisis massa. Praesent non pellentesque ligula, dapibus blandit mauris.
						</p>
 
						<p>
							Ut consequat nibh nec sapien auctor tristique. Duis vel viverra lectus. Nunc convallis non lectus et fermentum. 
							Donec dictum leo sit amet elit viverra vestibulum. Fusce elementum et arcu id cursus. Fusce volutpat, dolor ac 
							auctor viverra, odio mi facilisis turpis, sit amet aliquam leo odio in enim. Pellentesque at sem a risus laoreet 
							sollicitudin ac eu felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis 
							egestas. Nam tincidunt quam vel ante volutpat, dictum ornare quam consectetur. Nulla ut venenatis eros, 
							ut viverra risus. Donec blandit eget felis sit amet eleifend. Aliquam vulputate, ipsum ut fermentum dapibus, 
							dui odio bibendum risus, non maximus nulla nibh sed nisi. Mauris in neque odio. Pellentesque at blandit dolor. 
							Praesent finibus risus quis accumsan ultrices.
						</p>
 
						<p>
							Morbi risus nunc, facilisis vitae ligula sed, feugiat suscipit felis. Vivamus ornare erat et odio blandit, ut 
							euismod est rutrum. Praesent tortor libero, pharetra eget sem vitae, luctus vestibulum eros. Suspendisse 
							euismod purus odio, at sodales quam vehicula ut. Morbi nec nulla placerat, tincidunt est congue, faucibus ex. 
							Aenean est leo, varius ac tempus et, porttitor non odio. Aliquam rhoncus neque ac odio ultricies vulputate. 
							Praesent posuere facilisis placerat. Proin tempor enim nisi. Nulla nec urna porttitor, rutrum diam quis, ultrices velit. 
							Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent vel venenatis ex, 
							vel ultricies risus. Aliquam ac mauris vel mauris elementum accumsan vitae quis massa.a
						</p>
						
						<h2>Header 2 - H2</h2>
						<h3>Header 3 - H3</h3>
						<h4>Header 4 - H4</h4>
						<h5>Header 5 - H5</h5>
						<h6>Header 6 - H6</h6>

						<p>
							Nullam cursus, dui eget imperdiet dapibus, leo dui pretium libero, non facilisis massa felis et lacus. Suspendisse rutrum euismod turpis 
							vitae commodo. Sed in ante vel felis rutrum iaculis eget vitae ipsum. Praesent sollicitudin eros eu orci elementum porttitor. Aliquam efficitur 
							imperdiet volutpat. Pellentesque eget vestibulum dolor. Nunc sit amet pulvinar justo.
						</p>
						
						<h3>Links</h3>
						<p>
						
							<em>
								Note: styles apply to links in paragraphs/lists/blockquotes, or <code>&lt;a&gt;</code> tags with a class of <code>.inline</code>
							</em>
							
							<br />
							<br />
						
							<a href="#link">Link</a>
							<a href="#link" class="hover">Link:hover</a>
							<a href="#link" class="visited">Link:visited</a>
							<a href="#link" class="active">Link:active</a>
							
							<br />
							<br />
							
							And this is what a link will <a href="#link">look like</a> when it is written in a <a href="#link">regular paragraph</a>.
							
						</p>
						
						<hr />
						
						<ul>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ul>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ul>
							</li>
						</ul>
						
						<ol>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ol>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ol>
							</li>
						</ol>
						
						
						
					</div><!-- .article-body -->
				</div><!-- .content -->
				
				<div class="sidebar">
				
					<div class="archives-mod sidebar-mod">
						<h4 class="mod-title">Archives</h4>
						
						<div class="acc with-indicators">
						
							<div class="acc-item">
								<div class="acc-item-handle">
									2014 (11)
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
									<ul>
										<li><a href="#">October (2)</a></li>
										<li><a href="#">September (5)</a></li>
										<li><a href="#">August (6)</a></li>
									</ul>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									2013 (11)
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
									<ul>
										<li><a href="#">October (2)</a></li>
										<li><a href="#">September (5)</a></li>
										<li><a href="#">August (6)</a></li>
									</ul>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									2012 (11)
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
									<ul>
										<li><a href="#">October (2)</a></li>
										<li><a href="#">September (5)</a></li>
										<li><a href="#">August (6)</a></li>
									</ul>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->

							
						</div><!-- .acc -->
					</div><!-- .archives-mod -->
					
					<a class="callout-mod sidebar-mod bounce" href="#">
					
						<div class="lazybg img">
							<img src="../assets/dist/images/temp/callout-img.jpg" alt="dude with computer">
						</div><!-- .lazybg -->
						
						<div class="content">
							
							<h3>Hire a Retired Worker Today</h3>
							
							<p>Vivamus aliquet ex interdum vehicula</p>
							
							<span class="button red">Hire a Retired Worker Today</span>
							
						</div><!-- .callout-content -->
						
					</a><!-- .callout-mod -->

				
				</div><!-- .sidebar -->
				
			</div><!-- .main-body -->
		
		</div><!-- .sw -->
		
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>