			<footer class="d-bg">
			
				<div class="footer-nav">
					<div class="sw">
						<ul>
							<li><a href="#">Retired Workers</a></li>
							<li><a href="#">Employers</a></li>
							<li><a href="#">Employment</a></li>
							<li><a href="#">News</a></li>
						</ul>
						
						<?php include('i-social.php'); ?>
					</div><!-- .sw -->
				</div><!-- .footer-nav -->
			
				<div class="sw">
				
					<div class="footer-logo">
						<div>
							<a href="/" class="lazybg">
								<img src="../assets/dist/images/retired-workers-logo-white.svg" alt="Retired Workers Logoo White">
							</a><!-- .lazybg -->
						</div>
					</div><!-- .footer-logo -->
					
					<div class="footer-contact">
					
						<address>
							<span>123 Some Street</span>
							<span>This Town, PR</span>
							<span>A1B 2C3</span>
							
							<span>p. 555-555-5555</span>
							<span>f. 555-555-5555</span>
							
						</address>
					
					</div><!-- .footer-contact -->
					
					<div class="copyright">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Retired Workers</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Sitemap</a></li>
						</ul>						
					</div><!-- .copyright -->
					
					<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
				</div><!-- .sw -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
		
		<form action="/" method="get" class="global-search-form">
			<div class="fieldset">
				<input type="search" name="s" placeholder="Search Retired Workers...">
				<span class="close t-fa-abs fa-close toggle-search">Close</span>
			</div>
		</form><!-- .global-search-form -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/nape',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>