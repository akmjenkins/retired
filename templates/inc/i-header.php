<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>Retired Workface</title>
		<meta charset="utf-8">
		
		<!-- jQuery -->
		<script src="../assets/dist/lib/jquery/dist/jquery.min.js"></script>
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<!-- open sans -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700' rel='stylesheet'>

		<!-- slickslider -->
		<link rel="stylesheet" href="../assets/dist/lib/slick.js/slick/slick.css">
		<script src="../assets/dist/lib/slick.js/slick/slick.min.js"></script>
		
		<!-- magnific popup -->
		<link rel="stylesheet" href="../assets/dist/lib/magnific-popup/dist/magnific-popup.css">
		<script src="../assets/dist/lib/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
		
		<!-- tooltipster -->
		<link rel="stylesheet" href="../assets/dist/lib/tooltipster/css/tooltipster.css">
		<script src="../assets/dist/lib/tooltipster/js/jquery.tooltipster.min.js"></script>
		
		<link rel="stylesheet" href="../assets/dist/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">

		<!-- nav -->
		<?php include('i-nav.php'); ?>
	
		<div class="page-wrapper">	
			<header>
				<div class="sw">
					<a href="#" class="header-logo">
						<img src="../assets/dist/images/retired-workers-logo-white.svg" alt="Retired Workers Logo">
					</a>
				</div><!-- .sw -->
			</header>