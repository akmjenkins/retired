<div class="job-search-form">

	<form action="">
		<input type="text" name="s" placeholder="Search jobs...">
		
		<div class="selector with-arrow">
			<select name="location">
				<option value="">Location</option>
				<option value="1" data-tag="Loc. Name">A Very Long Location Name</option>
				<option value="2">Short Name</option>
				<option value="3">Alberta</option>
				<option value="4">Newfoundland</option>
			</select>
			<span class="value">&nbsp;</span>
		</div><!-- .selector -->
		
		<button>Search</button>
		
	</form>

</div><!-- .job-search-form -->