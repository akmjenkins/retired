<form action="/" class="body-form">
	<div class="fieldset">
	
		<div class="field-wrap t-fa-abs fa-user">
			<input type="text" name="name" placeholder="Full Name">
		</div>
	
		<div class="field-wrap t-fa-abs fa-envelope">
			<input type="email" name="email" placeholder="E-mail address">
		</div>
		
		<div class="field-wrap t-fa-abs fa-lock">
			<input type="password" name="password" placeholder="Password">
		</div>
		
		<div class="field-wrap t-fa-abs fa-lock">
			<input type="password" name="confirm_password" placeholder="Confirm Password">
		</div>
		
		<button class="red button">Submit</button>
	
	</div><!-- .fieldset -->
</form>
