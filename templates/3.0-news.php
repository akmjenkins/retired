<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">News</h1>
				<span class="hgroup-subtitle">Curabitur in Sapien Finibus</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">News</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">
		
			<div class="hgroup">
				<h2 class="hgroup-title">Featured News</h2>
				<span class="hgroup-subtitle">Curabitur in Sapien Finibus</span>
			</div><!-- .hgroup -->			
			
			<div class="featured-block">
				
				<div class="featured-img full">
					<div class="lazybg">
						<img src="../assets/dist/images/temp/featured-block-2.jpg" alt="dude with laptop">	
					</div><!-- .lazybg -->
				</div><!-- .featured-img -->
				
				<div class="featured-content">
				
					<div class="article-body">
						<div class="article-meta">
							<time>October 7, 2014</time>
						</div><!-- .article-meta -->
						
						<div class="hgroup">
							<h3 class="hgroup-title">Integer Sagittis et Lacus vel Sagittis</h3>
							<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
						</div><!-- .hgroup -->
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
						Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Curabitur lacus nisi, 
						placerat ut porta sollicitudin, hendrerit et felis. Fusce dolor eros, placerat eget diam at, fermentum 
						luctus lectus. Quisque augue tortor, euismod id augue in, mattis euismod urna.</p>
						
						<a href="#" class="button red">Keep Reading</a>
					</div><!-- .article-body -->
					
				</div><!-- .featured-content -->
				
			</div><!-- .featured-block -->
			
		</div><!-- .sw -->
		
	</section>
	
	<section class="filter-section">
	
		<div class="filter-bar">
			<div class="sw">

				<div class="filter-bar-left">
				
					<div class="count">
						Showing 6 of 10 Articles
					</div><!-- .tcount -->
					
				</div><!-- .filter-bar-left -->

				<div class="filter-bar-meta">
					
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
				</div><!-- .filter-bar-meta -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
		
			<div class="sw">
				
				<div class="grid eqh blocks collapse-at-850 blocks">
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button bounce" href="#">
							
								<div class="img-wrap">
									<div class="img lazybg" data-src="../assets/dist/images/temp/featured-block-2.jpg"></div>
								</div><!-- .img-wrap -->
								<div class="content">
								
									<div class="article-meta">
										<time>October 7, 2014</time>
									</div><!-- .article-meta -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button bounce" href="#">
							
								<div class="img-wrap">
									<div class="img lazybg" data-src="../assets/dist/images/temp/featured-block-2.jpg"></div>
								</div><!-- .img-wrap -->
								<div class="content">
								
									<div class="article-meta">
										<time>October 7, 2014</time>
									</div><!-- .article-meta -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->

					<div class="col-3 col sm-col-2">
						<div class="item">
						
							<a class="block with-button bounce" href="#">
							
								<div class="img-wrap">
									<div class="img lazybg" data-src="../assets/dist/images/temp/featured-block-2.jpg"></div>
								</div><!-- .img-wrap -->
								<div class="content">
								
									<div class="article-meta">
										<time>October 7, 2014</time>
									</div><!-- .article-meta -->
								
									<div class="hgroup">
										<h4 class="hgroup-title">Integer Sagittis Lacus Sagittis</h4>
										<span class="hgroup-subtitle">Phasellus Vitae Rutrum dui eu Fringilla</span>
									</div>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
									
									<span class="button">More Info</span>
									
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .sw -->

		</div><!-- .filter-content -->

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>